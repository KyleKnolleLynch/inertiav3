import { createResource, createSignal } from 'solid-js'
import styles from './App.module.css'
import Greeter from './components/greeter/Greeter'
import Clock from './components/clock/Clock'
import Todos from './components/todos/Todos'
import Settings from './components/settings/Settings'
import Footer from './components/footer/Footer'

import { SettingsProvider } from './context/SettingsContext'

//  Get current time/hours for dynamic image query
const [dynamicQuery, setDynamicQuery] = createSignal('')
const [height, setHeight] = createSignal(0)

const getHeight = () => {
  isNaN(window.innerHeight)
    ? setHeight(document.documentElement.clientHeight)
    : setHeight(window.innerHeight)
}

const setQuery = () => {
  const hourNow = new Date().getHours()

  if (hourNow < 4) {
    setDynamicQuery('night')
  } else if (hourNow < 12) {
    setDynamicQuery('nature+morning')
  } else if (hourNow < 18) {
    setDynamicQuery('nature+afternoon')
  } else if (hourNow < 20) {
    setDynamicQuery('nature+evening')
  } else {
    setDynamicQuery('night')
  }
}
setQuery()

const BASE_URL = 'https://api.unsplash.com/photos/random'
const UNSPLASH_CLIENT_ID = UNSPLASH_API_KEY
const API_URL = `${BASE_URL}?client_id=${UNSPLASH_CLIENT_ID}&query=${dynamicQuery()}&auto=format`

const getWallpaper = async () => {
  getHeight()
  const res = await fetch(API_URL)
 
  return res.json()
}

function App() {
  const [data] = createResource(getWallpaper)
 
  return (
    <div class={styles.App}>
      <Show
        when={data()}
        fallback={<div class={styles.fallback__wallpaper}></div>}
      >
        <div class={styles.wallpaper}>
          <picture>
            <source
              srcSet={data()?.urls.regular}
              media='(min-width: 600px)'
              width='1080'
              height={height()}
            />
            <img
              src={data()?.urls.small}
              alt={
                data()?.alt_description || data()?.description || 'Unsplash.com'
              }
              width='400'
              height={height()}
              class={styles.wallpaper__img}
            />
          </picture>
        </div>
      </Show>

      <SettingsProvider>
        <main class={styles.contentContainer}>
          <Greeter />
          <Clock />
          <Todos />
          <Settings />
          <Show when={data()} fallback={<div>Loading...</div>}>
            <Footer data={data()} />
          </Show>
        </main>
      </SettingsProvider>
    </div>
  )
}

export default App

import { createSignal, batch, For } from 'solid-js'
import { createLocalStore, removeIndex } from '../../utils'

import styles from './Todos.module.css'

export default function Todos() {
  const [todosSwitch, setTodosSwitch] = createSignal(false)
  const [newTitle, setTitle] = createSignal('')
  const [todos, setTodos] = createLocalStore('todos', [])

  const addTodo = e => {
    e.preventDefault()
    batch(() => {
      setTodos(todos.length, {
        title: newTitle(),
        done: false,
      })
      setTitle('')
    })
  }

  return (
    <section class={styles.todos}>
      <div class={`${styles.todos__modal} ${todosSwitch() && styles.opaque}`}>
        <h3>My Todos</h3>
        <form onSubmit={addTodo}>
          <input
            type='text'
            placeholder='Add new todo...'
            required
            value={newTitle()}
            onInput={e => setTitle(e.currentTarget.value)}
          />
          <button class={styles.todos__formBtn}>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='24'
              height='24'
              viewBox='0 0 24 24'
              fill='none'
              stroke='currentColor'
              stroke-width='2'
              stroke-linecap='round'
              stroke-linejoin='round'
              class={styles.todos__addSvg}
            >
              <circle cx='12' cy='12' r='10'></circle>
              <line x1='12' y1='8' x2='12' y2='16'></line>
              <line x1='8' y1='12' x2='16' y2='12'></line>
            </svg>
          </button>
        </form>
        <For each={todos}>
          {(todo, i) => (
            <div class={styles.todos__lineItem}>
              <input
                type='checkbox'
                checked={todo.done}
                onChange={e => setTodos(i(), 'done', e.currentTarget.checked)}
              />
              <input
                type='text'
                value={todo.title}
                onChange={e => setTodos(i(), 'title', e.currentTarget.value)}
              />
              <button
                onClick={() => setTodos(t => removeIndex(t, i()))}
                aria-label='Delete Todo'
                class={styles.todos__delete}
              >
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  width='24'
                  height='24'
                  viewBox='0 0 24 24'
                  fill='none'
                  stroke='currentColor'
                  stroke-width='2'
                  stroke-linecap='round'
                  stroke-linejoin='round'
                >
                  <rect x='3' y='3' width='18' height='18' rx='2' ry='2'></rect>
                  <line x1='9' y1='9' x2='15' y2='15'></line>
                  <line x1='15' y1='9' x2='9' y2='15'></line>
                </svg>
              </button>
            </div>
          )}
        </For>
      </div>

      <button
        aria-label='Toggle Todo List'
        onClick={() => setTodosSwitch(!todosSwitch())}
        class={styles.todos__toggle}
      >
        <svg
          xmlns='http://www.w3.org/2000/svg'
          width='24'
          height='24'
          viewBox='0 0 24 24'
          fill='none'
          stroke='currentColor'
          stroke-width='2'
          stroke-linecap='round'
          stroke-linejoin='round'
          class={styles.todos__svg}
        >
          <line x1='8' y1='6' x2='21' y2='6'></line>
          <line x1='8' y1='12' x2='21' y2='12'></line>
          <line x1='8' y1='18' x2='21' y2='18'></line>
          <line x1='3' y1='6' x2='3.01' y2='6'></line>
          <line x1='3' y1='12' x2='3.01' y2='12'></line>
          <line x1='3' y1='18' x2='3.01' y2='18'></line>
        </svg>
      </button>
    </section>
  )
}

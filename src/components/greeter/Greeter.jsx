import { createEffect, createSignal } from 'solid-js'
import { createStore } from 'solid-js/store'
import { useSettingsContext } from '../../context/SettingsContext'

import styles from './Greeter.module.css'

export default function Greeter() {
  const { settingsObj } = useSettingsContext()
  const [greeting, setGreeting] = createSignal('')
  const [greetingObj, setGreetingObj] = createStore({ name: '', focus: '' })

  createEffect(() => {
    const hourNow = new Date().getHours()
    hourNow < 12
      ? setGreeting('Good Morning')
      : hourNow < 18
      ? setGreeting('Good Afternoon')
      : setGreeting('Good Evening')
  })

  createEffect(() => {
    const prevName = localStorage.getItem('name')
    if (prevName) setGreetingObj('name', JSON.parse(prevName))

    const prevFocus = localStorage.getItem('focus')
    if (prevFocus) setGreetingObj('focus', JSON.parse(prevFocus))
  })

  const saveInput = (input, key) => {
    const i = JSON.stringify(input)
    localStorage.setItem(`${key}`, i)
  }

  return (
    <section class={styles.greeter}>
      <form onSubmit={e => e.preventDefault()}>
        <h1>
          <span>{greeting()}</span>
          <label>
            <input
              type='text'
              size={greetingObj.name.length || 14}
              aria-label='Enter your name'
              placeholder='{ Enter Name }'
              value={greetingObj.name}
              onInput={e => {
                setGreetingObj('name', e.target.value)
                saveInput(greetingObj.name, 'name')
              }}
              onKeyDown={e => e.key === 'Enter' && e.target.blur()}
            />
          </label>
        </h1>

        {settingsObj.showFocus && (
          <h2>
            <label>
              Daily Focus
              <input
                type='text'
                aria-label='Daily focus goal'
                placeholder=' '
                value={greetingObj.focus}
                onInput={e => {
                  setGreetingObj('focus', e.target.value)
                  saveInput(greetingObj.focus, 'focus')
                }}
                onKeyDown={e => e.key === 'Enter' && e.target.blur()}
              />
            </label>
          </h2>
        )}

        <input type='submit' class='hidden' />
      </form>
    </section>
  )
}

import { createSignal } from 'solid-js'
import { useSettingsContext } from '../../context/SettingsContext'

import styles from './Settings.module.css'

export default function Settings() {
  const { settingsObj, setSettingsObj } = useSettingsContext()
  const [settingsSwitch, setSettingsSwitch] = createSignal(false)

  const saveInput = (input, key) => {
    const i = JSON.stringify(input)
    localStorage.setItem(key, i)
  }

  return (
    <section class={styles.settings__section}>
      <fieldset
        class={`${styles.settings__panel} ${settingsSwitch() && styles.opaque}`}
      >
        <legend>Settings</legend>
        <label>
          <input
            type='checkbox'
            name='milTime'
            onClick={() => {
              setSettingsObj('milTime', c => !c)
              saveInput(settingsObj.milTime, 'milTime')
            }}
            checked={settingsObj.milTime}
          />
          24hr
        </label>
        <label>
          <input
            type='checkbox'
            name='showSeconds'
            onClick={() => {
              setSettingsObj('showSeconds', c => !c)
              saveInput(settingsObj.showSeconds, 'showSeconds')
            }}
            checked={settingsObj.showSeconds}
          />
          Seconds
        </label>
        <label>
          <input
            type='checkbox'
            name='showAmPm'
            onClick={() => {
              setSettingsObj('showAmPm', c => !c)
              saveInput(settingsObj.showAmPm, 'showAmPm')
            }}
            checked={settingsObj.showAmPm}
          />
          Am/Pm
        </label>
        <label>
          <input
            type='checkbox'
            name='showFocus'
            onClick={() => {
              setSettingsObj('showFocus', c => !c)
              saveInput(settingsObj.showFocus, 'showFocus')
            }}
            checked={settingsObj.showFocus}
          />
          Focus
        </label>
      </fieldset>

      <button
        class={styles.settings__cog}
        aria-label='Toggle Settings'
        onClick={() => setSettingsSwitch(!settingsSwitch())}
      >
        <svg
          xmlns='http://www.w3.org/2000/svg'
          width='24'
          height='24'
          viewBox='0 0 24 24'
          fill='none'
          stroke='currentColor'
          stroke-width='2'
          stroke-linecap='round'
          stroke-linejoin='round'
          class={`${settingsSwitch() && styles.rotateCog}`}
        >
          <circle cx='12' cy='12' r='3'></circle>
          <path d='M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z'></path>
        </svg>
      </button>
    </section>
  )
}

import { createSignal } from 'solid-js'
import { useSettingsContext } from '../../context/SettingsContext'

import styles from './Clock.module.css'

export default function Clock() {
  const { settingsObj } = useSettingsContext()
  const [clock, setClock] = createSignal({
    milHours: 0,
    hours: 0,
    minutes: 0,
    seconds: 0,
    datetime: '',
    amPm: '',
  })

  const getTime = () => {
    const time = new Date()
    setClock({
      milHours: time.getHours(),
      hours: time.getHours() % 12 || 12,
      minutes: time.getMinutes(),
      seconds: time.getSeconds(),
      datetime: time.toISOString(),
      amPm: time.getHours() >= 12 ? 'PM' : 'AM',
    })
  }

  getTime()
  setInterval(getTime, 1000)
  // return () => clearInterval(timerId)

  const addZero = num => {
    return (num < 10 ? '0' : '') + num
  }

  return (
    <section class={styles.clock}>
      <h2>
        <time dateTime={clock().datetime}>
          {settingsObj.milTime ? clock().milHours : clock().hours}:
          {addZero(clock().minutes)}
          {settingsObj.showSeconds && <span>:{addZero(clock().seconds)}</span>}
        </time>
        {settingsObj.showAmPm && !settingsObj.milTime && (
          <small class={styles.clock__amPm}>{clock().amPm}</small>
        )}
      </h2>
    </section>
  )
}

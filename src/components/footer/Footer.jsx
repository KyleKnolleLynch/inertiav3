import styles from './Footer.module.css'

export default function Footer(props) {
  const locationContent = (
    <>
      {!props.data.location.name &&
      !props.data.location.city &&
      !props.data.location.country ? (
        <span>Not provided</span>
      ) : (
        <span>
          {props.data.location?.name} {props.data.location?.city}{' '}
          {props.data.location?.country}
        </span>
      )}
    </>
  )

  const descriptionContent = props.data.description
    ? props.data.description
    : props.data.alt_description
    ? props.data.alt_description
    : 'No description provided'

  return (
    <footer class={styles.footer}>
      <div>
        <p class={styles.footer__description}>{descriptionContent}</p>
        <p class={styles.footer__location}>Location: {locationContent}</p>
      </div>
      <p>
        Photo by:{' '}
        <a href={props.data.user.links.html} target='_blank'>
          {props.data.user.name}
        </a>{' '}
        @{' '}
        <a href='https://unsplash.com/' target='_blank'>
          Unsplash
        </a>
      </p>
    </footer>
  )
}

import { createContext, createEffect, useContext } from 'solid-js'
import { createStore } from 'solid-js/store'

export const SettingsContext = createContext()

export function SettingsProvider(props) {
  const [settingsObj, setSettingsObj] = createStore({
    milTime: false,
    showSeconds: false,
    showAmPm: true,
    showFocus: true,
  })

  createEffect(() => {
    const milTime = localStorage.getItem('milTime')
    if (milTime) setSettingsObj('milTime', JSON.parse(milTime))
  
    const showSeconds = localStorage.getItem('showSeconds')
    if (showSeconds) setSettingsObj('showSeconds', JSON.parse(showSeconds))
  
    const showAmPm = localStorage.getItem('showAmPm')
    if (showAmPm) setSettingsObj('showAmPm', JSON.parse(showAmPm))
   
    const showFocus = localStorage.getItem('showFocus')
    if (showFocus) setSettingsObj('showFocus', JSON.parse(showFocus))
  })

  return (
    <SettingsContext.Provider value={{ settingsObj, setSettingsObj }}>
      {props.children}
    </SettingsContext.Provider>
  )
}

export function useSettingsContext() {
  return useContext(SettingsContext)
}

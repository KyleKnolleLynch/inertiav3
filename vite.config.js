import { defineConfig } from 'vite'
import solidPlugin from 'vite-plugin-solid'
import * as dotenv from 'dotenv'

const envDir = process?.cwd()
const envFile = '.env'

dotenv.config({ path: `${envDir}/${envFile}` })

export default defineConfig({
  plugins: [solidPlugin()],
  server: {
    port: 3000,
  },
  build: {
    target: 'esnext',
  },
  define: {
    UNSPLASH_API_KEY: `"${process.env.UNSPLASH_KEY}"` // wrapping in "" since it's a string
  },
})
